# ebook go samples Atmo

## Build and serve

```bash
cd services
subo build .
```

When the Bundle is created, serve it :

```bash
docker-compose up
```
