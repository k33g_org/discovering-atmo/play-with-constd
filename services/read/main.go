package main

import (
	"github.com/suborbital/reactr/api/tinygo/runnable" 
	"github.com/suborbital/reactr/api/tinygo/runnable/cache" 
	"github.com/suborbital/reactr/api/tinygo/runnable/req" 
)

type Read struct{}

func (h Read) Run(input []byte) ([]byte, error) {
	key := req.URLParam("key")
	value, _ := cache.Get(key)

	return value, nil
}

// initialize runnable, do not edit //
func main() {
	runnable.Use(Read{})
}
