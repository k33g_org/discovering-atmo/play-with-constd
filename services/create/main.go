package main

import (
	"github.com/suborbital/reactr/api/tinygo/runnable" 
	"github.com/suborbital/reactr/api/tinygo/runnable/cache" 
	"github.com/tidwall/gjson"
)

type Create struct{}

func (h Create) Run(input []byte) ([]byte, error) {

	key := gjson.GetBytes(input, "key").Str
	value := gjson.GetBytes(input, "value").Str

	
	cache.Set(key, value, 0)

	return []byte(key+":"+value+ " saved"), nil
}

// initialize runnable, do not edit //
func main() {
	runnable.Use(Create{})
}
