module gitlab.com/k33g/create

require (
	github.com/suborbital/reactr v0.14.0
	github.com/tidwall/gjson v1.14.0
)

require (
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
)

go 1.17
